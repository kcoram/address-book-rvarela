import { Component, OnInit } from '@angular/core';
import { User } from '../user';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users: User[] = [
    { userName: 'luke', fullName: 'Luke Skywalker'},
    { userName: 'han', fullName: 'Han Solo'},
    { userName: 'leia', fullName: 'Leia Organa'},
    { userName: 'ben', fullName: 'Ben Kenobi'},
    { userName: 'chewy', fullName: 'Chewbacca'},
    { userName: 'r2d2', fullName: 'R2-D2'},
    { userName: 'c3p0', fullName: 'C-3PO'},
    { userName: 'wedge', fullName: 'Wedge Antilles'}
  ];

  constructor() { }

  ngOnInit() {
  }

}
